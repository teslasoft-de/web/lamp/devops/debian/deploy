#!/bin/bash

#                                88
#                               8  8
# 888888 888888 .dP"Y8 88        ÅÅ    .dP"Y8  dP"Yb  888888 888888
#   88   88__   `Ybo." 88       dPYb   `Ybo." dP   Yb 88__     88
#   88   88""   o.`Y8b 88  .o  dP__Yb  o.`Y8b Yb   dP 88""     88
#   88   888888 8bodP' 88ood8 dP""""Yb 8bodP'  YbodP  88       88
# ________________________________________________________________
#|                                                                |
#|  deploy.sh - Web Project Deployment                            |
#|  Reads and executes deployment configurations                  |
#| -------------------------------------------------------------- |
#|  Manufactured by Teslasoft                                     |
#|                                                                |
#|  Project: Deploy - Teslasoft Project Deployment                |
#|  Company URL: www.teslasoft.de                                 |
#|  Author: Christian Kusmanow <christian.kusmanow@teslasoft.de>  |
#|  License: MIT                                                  |
#|  Copyright © 2007-2016 Teslasoft, All Rights Reserved          |
#|________________________________________________________________|

# This script performs simple git repository deployments for a
# specific branch, by reading the deployment setup parameters from
# environment configuration files located in the deployment working
# directory. (skeleton: deploy.default.sh)

# - Configuration File Naming Schemes -
# There are two naming schemes for configuration files available,
# because a deployment working directory can contain environments
# for only one or for different projects. This enables the deployment
# script to run deployments for specific projects and environments.
# 1. Singe Project: deploy.[ENV].sh
#    Parameter: -e|--env
# 2. Multi Project: [PROJECT].deploy.[ENV].sh
#    Parameters: -p|--project, -e|--env

# - Deployment Examples -

# Deploy all environments (deploy.*.sh):
# $ /srv/www/deploy.sh

# Deploy a specific environment (deploy.[ENV].sh):
# $ /srv/www/deploy.sh -e=staging

# Deploy all environments of a specific project ([PROJECT].deploy.*.sh):
# $ /srv/www/deploy.sh -p=myproject

# Deploy a environment of a specific project ([PROJECT].deploy.[ENV].sh):
# $ /srv/www/deploy.sh -p=myproject -e=staging

# Deploy and log (stdout):
# $ /srv/www/deploy.sh -p=myproject -e=staging -f > myproject.deploy.report.txt
# Deploy and log verbose (stdout + stderr)
# $ /srv/www/deploy.sh -p=myproject -e=staging -f 1>myproject.deploy.report.txt 2>&1

# TODO: Future development
# For heaving a proper task execution engine available, to provide a setup task
# monitoring, we'll integrate the 'Bash Infinity Framework' in the future.
# https://github.com/niieani/bash-oo-framework

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

# -----------------------------------------------------------
# Initialize Script Environment
# -----------------------------------------------------------
# Load the timer component for time measurement
. ${BASH_SOURCE[0]%/*}/timer.sh
DEPLOYMENT_START=$(timer.start)
shopt -s expand_aliases

# Register the top level shell for the TERM signal to exit.
# This can be used to send a TERM to the top level shell
trap "exit 1" TERM
export TOP_PID=$$
alias term_script="kill -s TERM ${TOP_PID}"

# Argument validation
# Usage: local var=`check_arg var $1`
function check_arg()
{
    # If the command is not empty echo complete parameter sequence beginning from second parameter
    [[ -n $2 ]] && echo ${@:2} && return
    >&2 echo Invalid function call \[${FUNCNAME[1]}\]: Missing argument \'$1\'.
    >&2 echo '    'Call Stack: ${FUNCNAME[*]} -\> ${BASH_SOURCE[0]}
    term_script
}

# -----------------------------------------------------------
# Basic Settings
# -----------------------------------------------------------

# CWD is the invoker PWD
CWD="$PWD" # script directory: "${BASH_SOURCE[0]%/*}"

# Skip . and .. entries from .* pattern
GLOBIGNORE=.:..

# Log Messages
alias log='echo Deploy – '
alias log_error='>&2 log'
alias log_ambiguous_opt='log_error Invalid option sequence. You can update, force or skip at once only. && term_script'
alias log_package='log Package Installation [${package}]: '
alias log_package_error='>&2 log_package Failed! && term_script'
alias log_git='log Git Clone [${project_domain}]: '
alias log_git_error='>&2 log_git'
alias log_symlink='log Create Symlink [${link}]: ${target}'
alias log_install='log Dependency Installation [${package}]: '
alias log_install_error='>&2 log_install Failed! && term_script'

# -----------------------------------------------------------
# Imports
# -----------------------------------------------------------

# Import package & gem install routines and file system utilities.
. ${BASH_SOURCE[0]%/*}/install.sh

# Web Root Initialization Script (if exists, executed after deployment in PROJECT_DIR)
WEB_ROOT_INIT=${BASH_SOURCE[0]%/*}/create_www_root.sh # $1=GIT_USER $2=PROJECT_DOMAIN

# That's all. Let's go!

# -----------------------------------------------------------
# Read Options
# -----------------------------------------------------------
show_opt()
{
    >&2 echo "Usage: deploy.sh [-OPTION]|[-OPTION]={VALUE}..."
    >&2 echo "[-p|--project]={PROJECT} - Project Name     - ([PROJECT].deploy.env.sh)"
    >&2 echo "[-e|--env]=[ENV]         - Environment Name - (project.deploy.[ENV].sh)"
    >&2 echo "[-f|--force]             - Force override of existing targets (reinstall)"
    >&2 echo "[-u|--update]            - Force update of existing targets"
    >&2 echo "[-s|--skip]              - Force skipping of existing targets (trigger setup only)"
    >&2 echo "{-h|--help}              - Display this help"

    term_script
}

for i in "$@"
do
case ${i} in
    -p=*|--project=*)
    OPT_PROJECT="${i#*=}"
    shift # past argument=value
    ;;
    -e=*|--env=*)
    OPT_ENV="${i#*=}"
    shift # past argument=value
    ;;
    -u|--update)
    [[ -n ${OPT_FORCE} ]] && log_ambiguous_opt
    [[ -n ${OPT_SKIP} ]] && log_ambiguous_opt
    OPT_UPDATE=true
    shift # past argument=value
    ;;
    -f|--force)
    [[ -n ${OPT_UPDATE} ]] && log_ambiguous_opt
    [[ -n ${OPT_SKIP} ]] && log_ambiguous_opt
    OPT_FORCE=true
    shift # past argument=value
    ;;
    -s|--skip)
    [[ -n ${OPT_UPDATE} ]] && log_ambiguous_opt
    [[ -n ${OPT_FORCE} ]] && log_ambiguous_opt
    OPT_SKIP=true
    shift # past argument=value
    ;;
    -h|--help)
    show_opt
    ;;
    *)
    log_error Unknown option \'${i}\'.
    show_opt
    ;;
esac
done

# -----------------------------------------------------------
# Environments Store
# -----------------------------------------------------------
declare -A ENVIRONMENT
ENVIRONMENTS=()

# Provide alias for registering environments
alias register_env='set_env ENVIRONMENT'

function set_env() {
    local store=$1 env=$2 conf=$3

    # Eval array into a new associative array
    local config="$( declare -p ${conf} )"
    eval "declare -A configuration=${config#*=}"

    # Store environment variables into the ENVIRONMENT array.
    for i in "${!configuration[@]}"; do
        if [[ -z "${configuration[$i]}" ]]; then
            log_error Invalid or empty value for key \'${i}\' in environment \'${conf}\'.
            log_error Loading of deployment configuration \'${env}\' failed. exiting...
            term_script
        fi
        eval "${store}[\$env|$i]=\${configuration[$i]}"
    done

    ENVIRONMENTS+=(${env})
}

function get_env()
{
    if [[ -z "${ENVIRONMENT[$1|$2]}" ]]; then
        log_error Invalid or empty value for key \'${2}\' in environment \'${1}\'.
        log_error Deployment processing failed. exiting...
        term_script
    fi
    echo "${ENVIRONMENT[$1|$2]}"
}

# -----------------------------------------------------------
# Load Environments
# -----------------------------------------------------------
# For security, we're not loading the environment configurations into the global space.
function load_env()
{
    # Lookup for environment configurations.
    cd ${CWD}

    local script_name="$(basename "$0")"
    local script_filename="${script_name%.*}"
    local script_extension="${script_name##*.}"
    [[ -n ${OPT_PROJECT} ]] && local project_name=${OPT_PROJECT}. || local project_name=
    [[ -n ${OPT_ENV} ]] && local env_name=${OPT_ENV}. || local env_name=*
    local file_pattern=./${project_name}${script_filename}.${env_name}${script_extension}

    for file in $(find ${file_pattern} -type f)
    do
        # Skip the default configuration
        [[ ${file} =~ "./*${SCRIPT_FILENAME}.default.${SCRIPT_EXTENSION}" ]] && continue

        chmod u+x ${file} && . ${file}
    done

    [[ "${#ENVIRONMENTS[@]}" = 0 ]] && log_error No Environements found in \'${CWD}\'. aborting... && term_script
}

# -----------------------------------------------------------
# Deploy Environments
# -----------------------------------------------------------
DEPLOY_COUNT=No
function deploy_env()
{
    local cwd=`get_env $1 'CWD'`
    local project_dir=`get_env $1 'PROJECT_DIR'`
    local project_domain=`get_env $1 'PROJECT_DOMAIN'`
    local git_user=`get_env $1 'GIT_USER'`
    local git_cwd=`get_env $1 'GIT_CWD'`
    local git_repository=`get_env $1 'GIT_REPOSITORY'`
    local git_branch=`get_env $1 'GIT_BRANCH'`
    local setup=`get_env $1 'SETUP'`

    # Store deployment start time
    local env_deploy_start=`timer.start`

    # Show deployment start
    echo
    echo ___________________________________________________________
    echo
    log ${project_domain} - \'$1\' Deployment
    echo
    echo -----------------------------------------------------------
    log "Start: `timer.utc ${env_deploy_start}`"
    echo -----------------------------------------------------------
    echo

    cd ${cwd}

    # Cleanup Project Directory
    if [[ -d ${project_dir} ]]; then
        if [[ -n ${OPT_UPDATE} ]]; then
            log_git Updating project dir \'${project_dir}\'.
            update_repository=true
        elif  [[ -n ${OPT_FORCE} ]]; then
            log_git Deleting project dir \'${project_dir}\'.
            rm -R ${project_dir}
        elif [[ -n ${OPT_SKIP} ]]; then
            log_git Skipping branch \'${git_branch}\'.
            skip_deploy=true
        else
            log_git Project location \'${cwd}${project_dir}\' exists.
            log_git Please select one of the following deployment options:
            log_git \(Note: If you skip, then only the environment setup \(\'${setup}\'\) will be triggered.\)

            # Read user choice
            select yn in 'Update (update)' 'Delete (delete)' 'Skip (skip)' 'Abort (abort)'; do
                case ${yn} in
                    'Update (update)' ) log_git Updating project dir \'${project_dir}\'. && update_repository=true; break;;
                    'Delete (delete)' ) log_git Deleting project dir \'${project_dir}\'. && rm -R ${project_dir}; break;;
                    'Skip (skip)' ) skip_deploy=true ; log_git Skipped; break;;
                    'Abort (abort)' ) log_git_error Aborted. No actions were taken.; term_script;;
                esac
            done
        fi
    fi

    if [[ -z ${skip_deploy} ]]; then

        # Clone Project Branch
        su ${git_user} -c bash <<EOF
# Save current PWD
cwd=$PWD

cd ${git_cwd}
if [ -z ${update_repository} ];then
    git clone ${git_repository} --branch ${git_branch} --single-branch ${project_dir}
    exit
fi
tmp=${project_dir}.tmp
git=${project_dir}.git
# Clone repository's .git folder into temp directory
git clone --no-checkout ${git_repository} --branch ${git_branch} --single-branch \${tmp} # might want --no-hardlinks for cloning local repo

# Replace old with new repository.
if [ -d \${git} ]; then rm -R \${git}; fi
mv \${tmp}/.git ${project_dir}

# Delete the temporary directory
rmdir \${tmp}

cd ${project_dir}

# Revert the state of the repo to HEAD.
git reset --hard HEAD
# Clean existing repository directories
# (Preserves directories without repository files. User group are also considered by git)
git clean -f

# Restore initial PWD
cd \${cwd}
EOF
        deployment_status='deployed'
    else
        deployment_status='skipped'
    fi

    # Save deployed directories count
    cd ${cwd}
    local deployed_dirs=`find ${project_dir}. -type d | wc -l`
    local deployed_files=`find ${project_dir}. -type f | wc -l`

    # Provide the setup task runner
    function run_setup()
    {
        local script=`check_arg script $1`
        local setup=${project_dir}private/${script}
        # TODO: Use bash-oo framework and track setup task status.
        chmod u+x ${setup} && log Executing project setup - ${setup}
    }

    # Run Setup
    chmod u+x ${file} && ${setup}

    # Save setup added directories count
    cd ${cwd}
    local setup_dirs=`find ${project_dir}. -type d | wc -l`
    local setup_files=`find ${project_dir}. -type f | wc -l`

    # Do web root initialization
    isDomain='\.([a-zA-Z]{2,7}|[0-9]{1,3})$' # RegEx: string ends with '.TLD'
    [[ ${project_domain} =~ $isDomain ]] && [[ -f ${WEB_ROOT_INIT} ]] && \
        cd ${cwd}${project_dir} && ${WEB_ROOT_INIT} ${git_user} ${project_domain}

    # Save web root initialization added directories count
    cd ${cwd}
    local webroot_dirs=`find ${project_dir}. -type d | wc -l`
    local webroot_files=`find ${project_dir}. -type f | wc -l`

    # Calculate deployment duration
    local env_deploy_end=`timer.start`

    # Show deployment status
    echo
    echo -----------------------------------------------------------
    log "End: `timer.utc ${env_deploy_end}`"
    echo -----------------------------------------------------------
    echo ___________________________________________________________
    echo
    log ${project_domain} - environment \'$1\' deployed!
    echo
    log Deployment Report:
    echo -----------------------------------------------------------
    echo "Git Repository:  ${git_repository}"
    echo "Git Branch:      ${git_branch} (${deployment_status})"
    echo "Git Dirs:        ${deployed_dirs} (${setup_dirs} after setup)"
    echo "Git Files:       ${deployed_files} (${setup_files} after setup)"
    echo "+Setup Dirs:     "$(( ${setup_dirs} - ${deployed_dirs} ))
    echo "+Setup Files:    "$(( ${setup_files} - ${deployed_files} ))
    echo "+Webroot Dirs:   "$(( ${webroot_dirs} - ${deployed_dirs} ))
    echo "+Webroot Files:  "$(( ${webroot_files} - ${deployed_files} ))
    echo "Deploy Time:     "`timer.time ${env_deploy_start} ${env_deploy_end}`
    echo
    echo Deployment Directory: ${cwd}${project_dir}
    (printf "PERMISSIONS LINKS OWNER GROUP SIZE MONTH DAY HH:MM DIR\n"; \
     ls -l ${cwd}${project_dir} | sed 1d) | column -t
    echo ___________________________________________________________
    DEPLOY_COUNT=$(( ${DEPLOY_COUNT} + 1 ))
}

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

# -----------------------------------------------------------
# Run Deployment
# -----------------------------------------------------------
load_env

for env in "${!ENVIRONMENTS[@]}"
do
    _env="${ENVIRONMENTS[env]}"
    [[ -n "${OPT_ENV}" && ! "${_env}" = "${OPT_ENV}" ]] && continue
    deploy_env "${_env}"
    [[ -n "${OPT_ENV}" && "${_env}" = "${OPT_ENV}" ]] && break
done

[[ ${DEPLOY_COUNT} = 1 ]] && DEPLOYMENT="deployment" || DEPLOYMENT="deployments"

log "${DEPLOY_COUNT} ${DEPLOYMENT}" done in $(timer.time ${DEPLOYMENT_START}). exiting...

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

# Var Validation Legend
#                        +-------+-------+-----------+
#                VAR is: | unset | empty | non-empty |
#+-----------------------+-------+-------+-----------+
#| [ -z "${VAR}" ]       | true  | true  | false     |
#| [ -z "${VAR+set}" ]   | true  | false | false     |
#| [ -z "${VAR-unset}" ] | false | true  | false     |
#| [ -n "${VAR}" ]       | false | false | true      |
#| [ -n "${VAR+set}" ]   | false | true  | true      |
#| [ -n "${VAR-unset}" ] | true  | false | true      |
#+-----------------------+-------+-------+-----------+