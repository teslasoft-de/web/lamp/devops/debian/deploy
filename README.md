# [Deploy][gitlab-url]
Deploy [Teslasoft] Projects under [Debian].

Standardized project deployment with logging and time measurement.

#### Abstract
Git repository and dependency deployment on debian, by using separate configurations for specific staging environments.

## Overview

### Feature List

Deploy has the following abilities:

#### Git Repository Deployment
 - [x] Impersonate (Run as)
 - [x] Repository exchange (fast synchronize)
 - [x] Logger
 - [x] Time Measurement

#### Package Install
 - [x] Debian Package Manager
 - [x] Ruby - RVM
 - [x] Node JS
 
#### Web Root Creation
 - [x] Structure
   - backup - Project backup
   - cgi-bin - Fast CGI
   - config - Project Configuration
   - log - Virtual Host Logging
   - private - Deployment and Operational Content
   - public - Web Root
 - [x] Permissions
 - [x] Time Measurement
 
### Future Planned Features
  - Deployment task execution and monitoring (Bash Infinity)
  - Multiple server rollout and monitoring
  - Deployment health check
  - Environment backup and restore
  
## Usage
### Template File Skeleton
[`deploy.default.sh`][deploy.default.sh]

Rename 'default|DEFAULT' in filename and its contents to the deployment environment.

```bash
#!/bin/bash
echo -----------------------------------------------------------
log Project - Default Deployment Configuration
echo -----------------------------------------------------------
# Instructions: Rename the strings 'Project', 'DEFAULT' and 'default'
# according to your project and environment name and remove this comment.

declare -A ENV_DEFAULT
ENV_DEFAULT=(
    ['CWD']=/home/teslasoft/www/
    ['PROJECT_DIR']=teslasoft.de/
    ['PROJECT_DOMAIN']=www.teslasoft.de
    ['GIT_USER']=teslasoft
    ['GIT_CWD']="~/www" # use quotes for heaving paths like "~/www" expanded inside the git user sub shell.
    ['GIT_REPOSITORY']=ssh://teslasoft@teslasoft.de/~/teslasoft.de # Git directories are also possible
    ['GIT_BRANCH']=master
    ['SETUP']=setup_default
)
register_env default ENV_DEFAULT

# Setup all dependencies after deployment.
function setup_default()
{
    #run_setup setup.sh
    return
}
```
### Deployment Examples

Deploy all environments (deploy.*.sh):
```sh
$ /srv/www/deploy.sh
```

Deploy a specific environment (deploy.[ENV].sh):
```sh
$ /srv/www/deploy.sh -e=staging
```

Deploy all environments of a specific project ([PROJECT].deploy.*.sh):
```sh
$ /srv/www/deploy.sh -p=myproject
```

Deploy a environment of a specific project ([PROJECT].deploy.[ENV].sh):
```sh
$ /srv/www/deploy.sh -p=myproject -e=staging
```

Deploy and log (stdout):
```sh
$ /srv/www/deploy.sh -p=myproject -e=staging -f > myproject.deploy.report.txt
```

Deploy and log verbose (stdout + stderr)
```sh
$ /srv/www/deploy.sh -p=myproject -e=staging -f 1>myproject.deploy.report.txt 2>&1
```

License
----
MIT

Author
----
2007-2017 Teslasoft - [Christian Kusmanow]

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job.)
   [teslasoft]: <https://git.teslasoft.de>
   [gitlab-url]: <https://git.teslasoft.de/devops/debian/deploy>
   [deploy.default.sh]: <https://git.teslasoft.de/devops/debian/deploy/blob/master/deploy.default.sh>
   [christian kusmanow]: <http://www.teslasoft.de/cv>
   
   [debian]: <https://www.debian.org/>