#!/bin/bash

#                                88
#                               8  8
# 888888 888888 .dP"Y8 88        ÅÅ    .dP"Y8  dP"Yb  888888 888888
#   88   88__   `Ybo." 88       dPYb   `Ybo." dP   Yb 88__     88
#   88   88""   o.`Y8b 88  .o  dP__Yb  o.`Y8b Yb   dP 88""     88
#   88   888888 8bodP' 88ood8 dP""""Yb 8bodP'  YbodP  88       88
# ________________________________________________________________
#|                                                                |
#|  install.sh - Debian installation utilities                    |
#| -------------------------------------------------------------- |
#|                                                                |
#|  Manufactured by Teslasoft                                     |
#|                                                                |
#|  Project: Deploy - Teslasoft Project Deployment                |
#|  Company URL: www.teslasoft.de                                 |
#|  Author: Christian Kusmanow <christian.kusmanow@teslasoft.de>  |
#|  License: MIT                                                  |
#|  Copyright © 2007-2016 Teslasoft, All Rights Reserved          |
#|________________________________________________________________|

# This script provides debian package installation utilities supporting
# the following:
# - Debian Package Manager (package_installed, install_package)
# - Ruby - RVM (command_installed, activate_ruby_gems, install_ruby_gems, install_ruby_gem)
# - Node JS (node_dependency_installed, install_node_dependency)
# - FS Utilities

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

# -----------------------------------------------------------
# Debian Package Manager
# -----------------------------------------------------------
function package_installed()
{
    local package=`check_arg package $1`
    if [ $(dpkg-query -W -f='${Status}' "${package}" 2>/dev/null | grep -c "ok installed") -gt 0 ]; then
        echo true
    fi
}

function install_package()
{
    local package=`check_arg package $1`
    local preInstall=$2
    local postInstall=$3

    # If installed display message and return
    [[ $(package_installed ${package}) ]] && log_package Installed && return
    # Pre installation tasks
    [[ -n ${preInstall} ]] && log_package pre install && ${preInstall}
    # Run Install
    log_package Installing... && aptitude install -y ${package}
    # Verify install
    [[ ! $(package_installed ${package}) ]] && log_package_error
    # Post installation tasks
    [[ -n ${postInstall} ]] && log_package post install && ${postInstall}
}

# -----------------------------------------------------------
# Ruby - RVM
# -----------------------------------------------------------
function command_installed()
{
    local command=`check_arg command $1`

    # Verify args not empty
    [[ -z $2 && -z $3 ]] && {
        >&2 echo Invalid function call \[$FUNCNAME\]: Invalid parameter sequence. Package name, version or both are required to detect the presence of command \'$1\'.
        >&2 echo '    'Call Stack: ${FUNCNAME[*]} -\> ${BASH_SOURCE[0]}
        term_script
    }
    local package=$2
    [[ -n $3 ]] && local version=$3 || local version=\\d+
    # Execute command by redirecting stderr to stdout (silent)
    # Grep RegEx: $package[ @.v]$version
    [[ $(echo `exec 2>&1 && ${command} && exec 1>&2` | grep -cP ${package}[\\s@\(\.v]*${version}) -gt 0 ]] && echo true
}

function activate_ruby_gems()
{
    source_rvm='source /usr/local/rvm/scripts/rvm'
    bashRC=.bashrc

    # Create bashrc if not present
    [[ ! -f ${bashRC} ]] && touch ~/${bashRC}

    # Add RVM import
    [[ $(cat ${bashRC} | grep -cG \"${source_rvm}\") -gt 0 ]] && \
        ${source_rvm} >> ${bashRC}

    # Execute RVM import
    ${source_rvm}
}

# Installs Ruby >= 2.0 using RVM
function install_ruby_gems()
{
    local package=rvm
    [[ -n $1 ]] && local version=$1 || local version=\\d+

    # If installed display message and return
    [[ $(command_installed "${package} -v" ${package} ${version}) ]] && log_install Installed && return
    # Run Install
    log_install Installing...

    # Install RVM GPG keys (for apt)
    curl -sSL https://rvm.io/mpapis.asc | gpg --import -
    # Install RVM (Ruby Version Manager)
    curl -sSL https://get.rvm.io | bash -s stable --ruby
    # Activate RVM
    activate_ruby_gems
    # Verify install
    [[ ! $(command_installed "${package} -v" ${package} ${version}) ]] && log_install_error

    # Install Ruby Gems
    rvm rubygems latest
}

function install_ruby_gem()
{
    local package=`check_arg package $1`
    [[ -n $2 ]] && local version=$2 || local version=\\d+
    # If already installed display message and return
    [[ $(command_installed "gem list ${package}" ${package} ${version}) ]] && log_install Installed && return
    # Run Install
    log_install Installing... && gem install ${package}
    # Verify install
    [[ ! $(command_installed "gem list ${package}" ${package} ${version}) ]] && log_install_error
}

# -----------------------------------------------------------
# Node JS
# -----------------------------------------------------------
function node_dependency_installed()
{
    local package=`check_arg package $1`
    if [ $(npm -g ls | grep -c ${package}) -gt 0 ]; then
        echo true
    fi
}

function install_node_dependency()
{
    local package=`check_arg package $1`

    # If installed display message and return
    [[ $(node_dependency_installed ${package}) ]] && log_package Installed && return
    # Run Install
    log_package Installing... && npm install -g ${package}
    # Verify install
    [[ ! $(node_dependency_installed ${package}) ]] && log_package_error
}

function install_compass()
{
    ### Install Ruby >= 2.0
    install_ruby_gems
    ## Install Gem Compass(SASS)
    install_ruby_gem compass
    # Install compass dependencies
    install_ruby_gem autoprefixer-rails
    # Install Gem Jacket
    install_ruby_gem jacket
    # Fix Jacket outdated dependency specifications
    jacket_gem_spec=/usr/local/rvm/gems/ruby-2.3.0/specifications/jacket-1.1.1.gemspec
    # Allow all versions greater or equal to the latest supported version (Replace ~> with >=)
    [[ ! -f ${jacket_gem_spec}.bak ]] && echo Fixing Jacket outdated dependency specifications && perl -pi.bak -e "s|\~>|>=|g" ${jacket_gem_spec}
}

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

# -----------------------------------------------------------
# FS Utilities
# -----------------------------------------------------------
function create_symlink()
{
    local target=`check_arg target $1`
    local link=`check_arg link $2`
    [[ -h ${link} ]] && return
    [[ -L ${link} ]] && return
    log_symlink
    ln -s ${target} ${link}
}