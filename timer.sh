#!/bin/bash

#                                88
#                               8  8
# 888888 888888 .dP"Y8 88        ÅÅ    .dP"Y8  dP"Yb  888888 888888
#   88   88__   `Ybo." 88       dPYb   `Ybo." dP   Yb 88__     88
#   88   88""   o.`Y8b 88  .o  dP__Yb  o.`Y8b Yb   dP 88""     88
#   88   888888 8bodP' 88ood8 dP""""Yb 8bodP'  YbodP  88       88
# ________________________________________________________________
#|                                                                |
#|  timer.sh - (Script Execution) Time Measurement and Formatting |
#| -------------------------------------------------------------- |
#|  Manufactured by Teslasoft                                     |
#|                                                                |
#|  Project: Deploy - Teslasoft Project Deployment                |
#|  Company URL: www.teslasoft.de                                 |
#|  Author: Christian Kusmanow <christian.kusmanow@teslasoft.de>  |
#|  License: MIT                                                  |
#|  Copyright © 2007-2016 Teslasoft, All Rights Reserved          |
#|________________________________________________________________|

# This script provides access to all time components of a given start date
# as UNIX timestamp concatenated with nanoseconds.
# Because currently there is no floating point support under Debian bash,
# it is not possible to create a date from a nanoseconds timestamp.
# The following does not work in bash like it does on other shells like zsh:
#   typeset -F SECONDS
#   $(date -u -d @${SECONDS} +"%T.%3N")
# So this script includes a manual workaround for having also accurate milliseconds available.

# Example:
# T=`timer.start`
# echo "Time in nanoseconds: ${T}"
# sleep 1
# `timer.time ${T}` // 00d 00:00:09s.311ms
# `timer.utc ${T}` // Saturday 14-06-2009 11:36:54:131 UTC

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

shopt -s expand_aliases
# Macro for $2 (end date) as local var
alias local_end='[[ -z ${2+set} ]] && local end=$(timer.start) || local end=$2'
# Macro for $2 (end date) in seconds as local var
alias local_second='local second="$((`timer.ns $1 ${end}`/1000000000))"'

function timer.start() # Creates a start time
{
    echo "$(date +%s%N)"
}

function timer.d()  # Returns the 'days' component of a given start time
{
    local_end && local_second
    echo "$((second/86400))"
}

function timer.h() # Returns the 'hours' component of a given start time
{
    local_end && local_second
    echo "$((second/3600%24))"
}

function timer.m() # Returns the 'minutes' component of a given start time
{
    local_end && local_second
    echo "$((second/60%60))"
}

function timer.s() # Returns the 'seconds' component of a given start time
{
    local_end && local_second
    echo "$((second%60))"
}

function timer.ms() # Returns the 'milliseconds' component of a given start time
{
    local_end && local ms="$((`timer.ns $1 ${end}`/1000000))"
    echo "$((ms%1000))"
}

function timer.ns() # Returns the time interval in nanoseconds of a given start time
{
    local_end
    echo "$(($end-$1))"
}

function timer.time() # Returns all time components of a given start date in debug time format.
{
    local_end
    printf "%02dd %02d:%02d:%02ds.%03dms\n" \
        `timer.d $1 ${end}` \
        `timer.h $1 ${end}` \
        `timer.m $1 ${end}` \
        `timer.s $1 ${end}` \
        `timer.ms $1 ${end}`
}

function timer.utc() # Returns the specified start time in UTC format.s
{
    local start=$1
    local seconds=$((${start} / 1000000000))
    local ms=$(timer.ms 0 ${start})

    # Create current date from unix timespan and append milliseconds
    echo `date -u -d @${seconds} +"%A %d-%m-%Y %I:%M:%S:${ms} %Z"`
}

# Get time nano version
#function timer.time()
#{
#    [[ -z ${2+set} ]] && local start=$(date +%s%N) || local start=$2
#    # Time interval in nanoseconds
#    T="$(($start-$1))"
#    # Seconds
#    S="$((T/1000000000))"
#    # Milliseconds
#    M="$((T/1000000))"
#
#    printf "%02dd %02d:%02d:%02ds.%03dms\n" "$((S/86400))" "$((S/3600%24))" "$((S/60%60))" "$((S%60))" "$((M%1000))"
#}