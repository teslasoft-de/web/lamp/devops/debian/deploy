#!/bin/bash
echo -----------------------------------------------------------
log Project - Default Deployment Configuration
echo -----------------------------------------------------------
# Instructions: Rename the strings 'Project', 'DEFAULT' and 'default'
# according to your project and environment name and remove this comment.

declare -A ENV_DEFAULT
ENV_DEFAULT=(
    ['CWD']=/home/teslasoft/www/
    ['PROJECT_DIR']=teslasoft.de/
    ['PROJECT_DOMAIN']=www.teslasoft.de
    ['GIT_USER']=teslasoft
    ['GIT_CWD']="~/www" # use quotes for heaving paths like "~/www" expanded inside the git user sub shell.
    ['GIT_REPOSITORY']=ssh://teslasoft@teslasoft.de/~/teslasoft.de # Git directories are also possible
    ['GIT_BRANCH']=master
    ['SETUP']=setup_default
)
register_env default ENV_DEFAULT

# Setup all dependencies after deployment.
function setup_default()
{
    #run_setup setup.sh
    return
}