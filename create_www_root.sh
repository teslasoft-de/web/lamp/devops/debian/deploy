#!/bin/bash

#                                88
#                               8  8
# 888888 888888 .dP"Y8 88        ÅÅ    .dP"Y8  dP"Yb  888888 888888
#   88   88__   `Ybo." 88       dPYb   `Ybo." dP   Yb 88__     88
#   88   88""   o.`Y8b 88  .o  dP__Yb  o.`Y8b Yb   dP 88""     88
#   88   888888 8bodP' 88ood8 dP""""Yb 8bodP'  YbodP  88       88
# ________________________________________________________________
#|                                                                |
#|  create_www_root.sh - Setup Web Root Directory Structure       |
#|  Creates apache/cgi web root structures                        |
#| -------------------------------------------------------------- |
#|                                                                |
#|  Manufactured by Teslasoft                                     |
#|                                                                |
#|  Project: Deploy - Teslasoft Project Deployment                |
#|  Company URL: www.teslasoft.de                                 |
#|  Author: Christian Kusmanow <christian.kusmanow@teslasoft.de>  |
#|  License: MIT                                                  |
#|  Copyright (c) 2007-2016 Teslasoft, All Rights Reserved        |
#|________________________________________________________________|

# This script will create directories and files only if they do not
# exist. File system permissions will always be set. You can run this
# script every time when the web contents have been changed, for
# applying the desired file system permissions to all elements again.

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

# -----------------------------------------------------------
# Initialize Script Environment
# -----------------------------------------------------------
# CWD is the invoker PWD

. ${BASH_SOURCE[0]%/*}/timer.sh

#-----------------------------------------------
# Create HTTP Root
#-----------------------------------------------
if [ -z "$1" ]; then
    echo "ERROR: no group specified"
    exit 1
fi

if [ -z "$2" ]; then
    echo "ERROR: no domain specified"
    exit 1
fi

function createDir()
{
    [[ ! -d $1 ]] && mkdir $1
    chown $2:$3 $1 && chmod $4 $1
}
function createFile()
{
    [[ ! -f $1 ]] && touch $1
    chown $2:$3 $1 && chmod $4 $1
}
function setPermissions()
{
    # Skip . and .. entries from .* pattern
    GLOBIGNORE=.:..

    local start=`timer.start`

    # Set user/group
    printf "Setting owner for \'$1\' to www-data:$2 ..."
    find $1/. -type d -exec chown www-data:$2 {} \;
    find $1/. -type f -exec chown www-data:$2 {} \;
    echo "done (`timer.time ${start}`)"

    start=`timer.start`

    # Set file system permissions
    printf "Setting permissions for \'$1\' to dir:$3 file:$4 ..."
    find $1/. -type d -exec chmod $3 {} \;
    find $1/. -type f -exec chmod $4 {} \;
    echo "done (`timer.time ${start}`)"
}

#ÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅÅ

# Backup (Owner group only)
createDir backup $1 $1 750

# CGI Binaries (Web readonly)
createDir cgi-bin www-data $1 570

# Config Dir
createDir config www-data $1 570

# Server Logs
createDir log www-data $1 550
createFile log/$2.php_error.log www-data $1 264
createFile log/$2.ssl_php_error.log www-data $1 264
createDir log/old $1 $1 750

# Private working directory
createDir private $1 $1 750

# Web Root
createDir public www-data $1 570

# File System Permissions
setPermissions public $1 774 664
